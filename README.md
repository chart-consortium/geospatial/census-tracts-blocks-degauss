# Census Tracts Blocks Degauss

A repo to build a container for running [Degauss census tracts/block groups](https://degauss.org/census_block_group/) to assign addresses to census tracts and block groups based on geocoded locations. The container is bootstrapped from the ['census block group' image](https://github.com/degauss-org/census_block_group), can run RStudio Server and has additional tools including tidyverse and the 'sf' R package.

## Building

- A new container build will only be initiated by a modification to the Singularity.def file
- The built container file will only be pushed to the Duke OIT registry when a commit is tagged (ideally following the current version naming convention)
    + Note that to push, the container must be re-built first.

## Pulling

To pull the latest version of the container file via Singularity, use the following command where "/path/to/container/directory" is the directory where the container will be pulled to:

```
singularity pull --force --dir /path/to/container/directory oras://gitlab-registry.oit.duke.edu/chart-consortium/geospatial/census-tracts-blocks-degauss/census-tracts-blocks-degauss:latest
```

Alternatively, the latest container version can be pulled using wget (to the current directory by default):

```
wget --no-check-certificate https://research-singularity-registry-public.oit.duke.edu/chart-consortium/census-tracts-blocks-degauss.sif
```

## Running

The container can be run using something like the following, where "path to geocoded addresses" is the path to CSV file with addresses:

```
export ADDRESS_FILE="path to geocoded addreses"
singularity run census-tracts-blocks-degauss.sif ${ADDRESS_FILE}
```

## Notes


